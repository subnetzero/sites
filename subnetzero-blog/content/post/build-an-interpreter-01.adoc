+++
categories = ["rust", "iridium", "development", "languages"]
date = "2018-07-17T19:59:50-07:00"
description = "Covers getting a basic VM started and design goals"
slug = "building-language-vm-part-01"
tags = ["rust","iridium"]
title = "So You Want to Build a Language VM - Part 01 - Overview and a Simple VM"
draft = "false"
author = "Fletcher Haynes <fletcher@subnetzero.io>"
series = "iridium-vm"
part = 1
+++

== Introduction
Hi there! This is the first post in a series on building a VM/interpreter for a programming language in Rust. Its something I've been tinkering with for a few months so am turning them into a tutorial series.

=== Why?
It is a topic in CS that has always interested me. When I began to write Elixir and Erlang code last year, I was impressed with the BEAM VM. Not just for executing code, either, but for its abilities as a command and control platform for applications, built-in clustering, and other features. I come from an infrastructure background, and one of my pet theories is that a lot of what we rely on tools like Docker for could be better served by the language VM.

=== Inspiration and Credits
There were a few websites, books, and projects I referenced heavily while working on this:

1. Thorsten Ball has an awesome book, https://interpreterbook.com[`Writing an Interpreter in Go`], that walks you through writing a tree-walking interpreter in Go. It is thorough, well-written, and a great introduction to the topic.
  * There is also a Rust version of that interpreter: https://github.com/Rydgel/monkey-rust
2. The BEAM VM (https://en.wikipedia.org/wiki/BEAM_(Erlang_virtual_machine))
3. The Lua VM (http://files.catwell.info/misc/mirror/lua-5.2-bytecode-vm-dirk-laurie/lua52vm.html)
4. The Wren VM (https://github.com/munificent/wren and http://wren.io)
5. The Dis VM (http://www.vitanuova.com/inferno/papers/dis.html)

=== Code
You can find the code at https://gitlab.com/subnetzero/iridium.

=== Goals
We want our VM to be:

1. Reasonably performant compared to modern implementations. We'll use Python as our comparison.
2. Fault tolerant.
3. Serves as command and control platform for running applications.
4. Clustering of VMs running on different physical servers.

== Types
Interpreters fall in to one of three categories:

1. Tree-walking
2. Stack-based
3. Register-based

[IMPORTANT]
If you want a refresher on what a CPU is, assembly, etc, check out {{< ref "build-an-interpreter-00" >}}

=== Tree Walking Interpreters
A tree-walking interpreter transforms your source code into a tree-like data structure, starts at the root of the tree and visits each node, performing operations as it goes along. These are often the first interpreters someone codes.

==== Benefits
* Simple
* Allows greater flexibility to further transform the code because it is already in a data structure

==== Drawbacks
* Slow compared to the other types

=== Stack Based Interpreters
These are the most common. The JVM is one, the Python VM is another. These VMs store operations and results on a stack. When the VM executes an instruction, it pops the most recent value off the stack, performs the operation, and pushes the result back on.

==== Benefits
* Easier to code than a register-based VM
* Decent performance

==== Drawbacks
* Does not map to real hardware; CPUs use registers.
* Most tasks require more instructions

=== Register Based Interpreters
These are the last type, and are the least common. Examples include the BEAM VM (sometimes called the Erlang VM), the Lua VM and
the Dalvik VM (the thing that runs Java code on Android).

==== Benefits
* Register-based VMs are much closer to how actual hardware works
* More performant

==== Drawbacks
* More complex to code
* We have to worry about register allocation and de-allocation when writing our compiler

== So We're Coding...which?
...a register based VM!

Why? They're more interesting to me. The Internet has quite a few tutorials on how to implement a tree-walking or stack-based virtual machine. There's far fewer for register based VMs.

Don't worry, it'll be fun! =)

== Assembly
The next step up from raw 0s and 1s in terms of language abstraction. Here's an approximate hierarchy/timeline of computer languages:

1. Binary
2. Assembly
3. FORTRAN, C, and later languages like Rust
  * Called "high-level languages" when they first came out, we now often refer to them as low-level languages.
4. Perl, Bash, Python, Ruby, and later languages like Go
  * These are the "very high-level" languages
  * Yes, Go could fit into the C level languages section too
5. SQL and other DSL (Domain Specific Languages)
  * Also Excel macros and other nightmarish horrors

As we work through this project, we're going to create an assembly language and assembler so we can write programs in something other than a hex editor. Then later on, we'll work on an even higher level language that compiles down to our assembly, and then down to our bytecode.

NOTE: If you have some experience with assembly, know the one we will be writing is a RISC-style, MIPS-inspired assembly, because it seemed simplest.

=== Structure of an Assembly Instruction
Our virtual CPUs will have the ability to take 32 bits of data at a time, execute it, and then go get another group of 32 bits. At a very general level, that is all hardware processors do:

1. Read next instruction
2. Execute instruction
3. Repeat

They just do it _really, really_ fast.

==== Breaking Up is Hard to Do
We can break our 32-bit instructions out into smaller blocks. This lets us put multiple arguments in one line:

{{< highlight asm >}}
LOAD $0 #500
{{< /highlight >}}
and not have to do:
{{< highlight asm >}}
LOAD
$0
#500
{{< / highlight >}}

[NOTE]
The smallest unit of bits we are going to work with right now is 8, so a single byte. A later enhancement will let us use more exotic bit numbers.

==== Instructions
So, a quick terminology review. A grouping of 32-bits is an _Instruction_. The first 8 bits of those 32 will contain our _Opcode_. The remaining bits will be dedicated to _Operands_. With this design, an _Opcode_ can have 0, 1, 2, or 3 _Operands_.

The table below shows all the permutations we'll be using:
[cols="4*^", width="75%",options=noheader]
|===

4+^|Opcode (8 bits)

|Opcode (8 bits)
3+^|Operand 1 (24 bits)

|Opcode (8 bits)
|Operand 1 (8 bits)
2+^|Operand 2 (16 bits)

|Opcode (8 bits)
|Operand 1 (8 bits)
|Operand 2 (8 bits)
|Operand 3 (8 bits)

|===

This shows the ways to use our 32 bits we have available for an instruction. The HLT instruction (or `opcode`), which stops execution, has no operands, so we use 1 8-bit operator. The remaining 24 bits will be zero'd. Throughout the tutorial, I'll show the bytecode in hex, laid out like so:

.HLT Example
----
0x06 00 00 00
----

HLT is just the number 6, and we need no operands, so this is one complete instruction our VM could execute. Everything else is just building on top of this.

== Starting the Project
Let's begin by creating a new Rust project with cargo. The rest of this tutorial assumes you have installed the latest version of Rust and the `cargo` executable is on your PATH. If you haven't, go to https://rustup.rs/ to get setup.

{{< highlight bash >}}
$ cargo new iridium --bin
{{< / highlight >}}

Hop into the newly created iridium directory and make a new file under the `src/` directory called `vm.rs`:

{{< highlight bash >}}
$ cd iridium
$ touch src/vm.rs
{{< / highlight >}}

You can open `vm.rs` in whatever editor you prefer (which should be vim, of course).

=== In the Code
OK, let's start coding! Remember, all we're doing is writing a piece of software that emulates the functionality of a hardware CPU. So let's start with a simple
struct.

{{< highlight rust >}}
pub struct VM {

}
{{< / highlight >}}

First off, let's give our little VM some registers. We can represent the registers with an array:

{{< highlight rust >}}
pub struct VM {
    registers: [i32; 32]
}
{{< / highlight >}}

[NOTE]
Why use an array and not a vector? Because we know the number we need at compile time.

And then in the implementation block for the struct:

{{< highlight rust >}}
impl VM {
    pub fn new() -> VM {
        VM {
            registers: [0; 32]
        }
    }
}
{{< / highlight >}}

This will initialize all registers to zero whenever we create a VM.

=== Tests
Before we go much further, let's write some tests. After all, we wouldn't want to be like _those_ developers who never write tests, would we?

In `src/vm.rs`, at the end, put the following:

{{< highlight rust >}}
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_create_vm() {
        let test_vm = VM::new();
        assert_eq!(test_vm.registers[0], 0)
    }
}
{{< / highlight >}}

Next, add the following in `src/main.rs` at the top:

{{< highlight rust >}}
pub mod vm;
{{< / highlight >}}

(You can also empty out the main function from the default of printing `Hello World!`)

You should now be able to run `cargo test` from the main directory and see one test
run and pass. Yay! We're on our way! In the next section, we'll do our first opcode!

Ready? https://blog.subnetzero.io/post/building-language-vm-part-02/
