---
title: "Iridium"
description: "A series of tutorials for building register-based VM for a high-level language written in Rust"
link: "https://blog.subnetzero.io"
tags: ["rust", "iridium"]
weight: 1
draft: false
type: "project"
---
